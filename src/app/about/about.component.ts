import { Component, OnInit } from '@angular/core';
import { FootyServiceService } from '../footy-service.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  teams = [];
  sources = [];

  constructor(private dataService: FootyServiceService) { }

  ngOnInit(): void {
    this.getSources();
    this.getTeams();
  }

  getTeams() {
    this.dataService.getTeams().subscribe(temp => {
      temp.forEach(x => {
        this.teams.push({name:x.name,logo:x.logo});
      })
    })
    console.log(this.teams)
  }

  getSources(){
    this.dataService.getSources().subscribe(temp=>{
      temp.forEach(x=>{        
          this.sources.push(x);
      })     
      
    })
    
  }

}

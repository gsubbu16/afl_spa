export class Standings{
    constructor(
        public losses: number,
        public name: string,
        public against: number,
        public behinds_for: number,
        public wins: number,
        public pts: number,
        public goals_against: number,
        public played: number,
        public percentage: number,
        public behinds_against: number,
        public rank: number,
        public goals_for: number,        
        public draws: number,
        public id: number       
    ) { }
};
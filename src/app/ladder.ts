export class Ladder{
    constructor(
        public percentage: number,
        public mean_rank: number,
        public swarms: number[ ],
        public sourceid: number,
        public teamid: number,
        public year: number,
        public round: number,
        public source: string,
        public rank: number,
        public updated: string,
        public wins: number,
        public team: string   
    ) { }
};
import { Component, OnInit } from '@angular/core';
import { FootyServiceService } from '../footy-service.service';
import { Team } from '../team';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {

  stats = [];
  teams = [];

  statsName: string = " ";
  showRows: boolean = true;

  constructor(private dataService: FootyServiceService) { }

  ngOnInit(): void {
    this.getTeams();
  }

  getStats(): void {
    if (this.statsName == " ") {
      alert("Select a team to continue!")
      this.showRows = false;
      return;
    }

    this.showRows = true;    
    this.dataService.getGames().subscribe(temp => {
      var homeMatches = [];
      var awayMatches = [];

      temp.forEach(x => {
        //As home team
        if (x.hteam == this.statsName && x.year == 2019)
          homeMatches.push(x);
        //As away team
        if (x.ateam == this.statsName && x.year == 2019)
          awayMatches.push(x);
      })
      //swap away to home 
      let swapteam = " ";
      awayMatches.forEach(x => {
        swapteam = x.ateam;
        x.ateam = x.hteam;
        x.hteam = swapteam;
      })
      //sort & push the team's data
      this.stats = (homeMatches.concat(awayMatches)).sort((x, y) => new Date(x.date).getTime() - new Date(y.date).getTime());

    });
  }

  getTeams() {
    this.dataService.getTeams().subscribe(temp => {
      temp.forEach(x => {
        this.teams.push(x.name);
      })
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { FootyServiceService } from '../footy-service.service';

@Component({
  selector: 'app-standings',
  templateUrl: './standings.component.html',
  styleUrls: ['./standings.component.css']
})
export class StandingsComponent implements OnInit {

  standings = [];

  constructor(private dataService: FootyServiceService) { }

  ngOnInit(): void {
    this.getStandings();
  }

  getStandings():void {
    this.dataService.getStandings().subscribe(temp => {
      
      temp.forEach(x=>{
        this.standings.push(x);
      })
      //round off 2 decimals for percentage
      this.standings.forEach(x=>{
        x.percentage = x.percentage.toFixed(2);
      })
      
    })
  }

}

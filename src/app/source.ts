export class Source{
    constructor(
        public name: string,
        public id: number,
        public icon: string,
        public url:string
    ){}
};
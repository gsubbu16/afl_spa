import { Component, OnInit } from '@angular/core';
import { FootyServiceService } from '../footy-service.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  myTeam: string = " ";
  rivalTeam: string = " ";
  showRows: boolean = false;

  games: {
    hteam: string, ateam: string, venue: string, hgoals: number,
    agoals: number, round: number, winner: string, year: number
  }[] = [];

  teams = [];

  constructor(private dataService: FootyServiceService) { }

  ngOnInit(): void {
    this.getTeams();
  }

  getGames(): void {

    if (this.myTeam == this.rivalTeam || (this.myTeam == " " || this.rivalTeam == " ")) {
      if (this.myTeam == this.rivalTeam)
        alert("Same team can't be selected!")
      else
        alert("Select a team to continue");
      this.myTeam = " ";
      this.rivalTeam = " ";
      this.showRows = false;
      return;
    }


    this.showRows = true;
    this.dataService.getGames().subscribe(temp => {

      var homeGames = [];
      var awayGames = [];

      temp.forEach(x => {
        //On Home Ground
        if (x.hteam == this.myTeam && x.ateam == this.rivalTeam && x.year == 2019) {
          if (x.winner == null) {
            x.winner = "Not played yet";
            x.hgoals = 0;
            x.agoals = 0;
          }

          homeGames.push({ hteam: x.hteam, ateam: x.ateam, venue: x.venue, hgoals: x.hgoals, agoals: x.agoals, round: x.round, winner: x.winner, year: x.year });
        }
        //On Away Ground - swapping 
        if (x.hteam == this.rivalTeam && x.ateam == this.myTeam && x.year == 2019) {
          if (x.winner == null) {
            x.winner = "Not played yet";
            x.hgoals = 0;
            x.agoals = 0;
          }

          awayGames.push({ hteam: x.ateam, ateam: x.hteam, venue: x.venue, hgoals: x.agoals, agoals: x.hgoals, round: x.round, winner: x.winner, year: x.year });
        }
      })

      //sort by date and push
      this.games = homeGames.concat(awayGames);
      this.games = this.games.sort((x, y) => x.year - y.year);
    })
  };

  getTeams() {
    this.dataService.getTeams().subscribe(temp => {
      temp.forEach(x => {
        this.teams.push(x.name);
      })
    })
  }

}

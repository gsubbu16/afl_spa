import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { PredictionsComponent } from './predictions/predictions.component';
import { ResultsComponent } from './results/results.component';
import { FixturesComponent } from './fixtures/fixtures.component';
import { StatsComponent } from './stats/stats.component';
import { LadderComponent } from './ladder/ladder.component';
import { StandingsComponent } from './standings/standings.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { RoundResultsComponent } from './round-results/round-results.component';
import { StadiumsComponent } from './stadiums/stadiums.component';


const routes: Routes = [
   { path: '', component: HomeComponent },
   { path: 'about', component: AboutComponent },
   { path: 'predictions', component: PredictionsComponent },
   { path: 'results', component: ResultsComponent },
   { path: 'roundResults', component: RoundResultsComponent },
   { path: 'fixtures', component: FixturesComponent },
   { path: 'stats', component: StatsComponent },
   { path: 'ladder', component: LadderComponent },
   { path: 'standings', component: StandingsComponent },
   { path: 'stadiums', component: StadiumsComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Project';

  selectComp(event: any) { 
    $('ul li a').removeClass('activenavbar');
    $('#' + event.target.attributes.id.value).addClass('activenavbar');    
  }
}

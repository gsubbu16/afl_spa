import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Tip } from './tip';
import { Games } from './games';
import { Team } from './team';
import { Ladder } from './ladder';
import { Standings } from './standings';
import { Source } from './source';

@Injectable({
  providedIn: 'root'
})
export class FootyServiceService {

  constructor(private http: HttpClient) { }

  getTeams(): Observable<Team[]> { 

    return this.http.get('https://api.squiggle.com.au/?q=teams').pipe(
      map((data: any) => data.teams.map((item: any) => new Team(
        item.id,
        item.name,
        item.logo,
        item.abbrev
      )))
    );
  }

  getGames(): Observable<Games[]> {

    return this.http.get('https://api.squiggle.com.au/?q=games').pipe(
      map((data: any) => data.games.map((item: any) => new Games(
        item.complete,
        item.is_grand_final,
        item.tz,
        item.hbehinds,
        item.ateam,
        item.winnerteamid,
        item.hgoals,
        item.updated,
        item.round,
        item.is_final,
        item.hscore,
        item.abehinds,
        item.winner,
        item.ascore,
        item.hteam,
        item.ateamid,
        item.venue,
        item.hteamid,
        item.agoals,
        item.year,
        item.date,
        item.id
      )))
    );
  }

  getTips(): Observable<Tip[]> {

    return this.http.get('https://api.squiggle.com.au/?q=tips;year=2019').pipe(
      map((data: any) => data.tips.map((item: any) => new Tip(
        item.confidence,
        item.bits,
        item.gameid,
        item.ateamid,
        item.venue,
        item.year,
        item.correct,
        item.date,
        item.updated,
        item.hteam,
        item.tipteamid,
        item.margin,
        item.err,
        item.tip,
        item.ateam,
        item.source,
        item.hconfidence,
        item.hteamid,
        item.round
      )))
    );
  }

  getStandings(): Observable<Standings[]> {
    return this.http.get('https://api.squiggle.com.au/?q=standings').pipe(
      map((data: any) => data.standings.map((item: any) => new Standings(
        item.losses,
        item.name,
        item.against,
        item.behinds_for,
        item.wins,
        item.pts,
        item.goals_against,
        item.played,
        item.percentage,
        item.behinds_against,
        item.rank,
        item.goals_for,
        item.draws,
        item.id

      )))
    );
  }

  getLadder(): Observable<Ladder[]> {
    return this.http.get('https://api.squiggle.com.au/?q=ladder').pipe(
      map((data: any) => data.ladder.map((item: any) => new Ladder(
        item.percentage,
        item.mean_rank,
        item.swarms,
        item.sourceid,
        item.teamid,
        item.year,
        item.round,
        item.source,
        item.rank,
        item.updated,
        item.wins,
        item.team
      )))
    );
  }

  getSources(): Observable<Source[]> {
    return this.http.get('https://api.squiggle.com.au/?q=sources').pipe(
      map((data: any) => data.sources.map((item: any) => new Source(
        item.name,
        item.id,
        item.icon,
        item.url
      )))
    );
  }
}

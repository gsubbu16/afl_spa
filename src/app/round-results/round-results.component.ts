import { Component, OnInit } from '@angular/core';
import { FootyServiceService } from '../footy-service.service';

@Component({
  selector: 'app-round-results',
  templateUrl: './round-results.component.html',
  styleUrls: ['./round-results.component.css']
})
export class RoundResultsComponent implements OnInit {

  showRows: boolean = true;
  year: number = 0;
  round: number = 0;

  games: {
    hteam: string, ateam: string, venue: string, hgoals: number,
    agoals: number, round: number, winner: string, year: number
  }[] = [];

  results = [];
  yearsArr = [];
  roundsArr = [];

  constructor(private dataService: FootyServiceService) { }

  ngOnInit(): void {
    this.getYearRounds();
  }
  getYearRounds() {
    this.dataService.getGames().subscribe(temp => {
      temp.forEach(x => {
        if (x.year > 2015)
          this.yearsArr.push(x.year);
      })

      temp.forEach(x => {
        this.roundsArr.push(x.round);
      })
      //Get Years & sort
      this.yearsArr = this.yearsArr.filter((elem, i, arr) => {
        if (arr.indexOf(elem) === i) {
          return elem
        }
      })
      this.yearsArr.sort((x, y) => x.year - y.year);
      //Get Rounds
      this.roundsArr = this.roundsArr.filter((elem, i, arr) => {
        if (arr.indexOf(elem) === i) {
          return elem
        }
      })

      console.log(this.yearsArr);
      console.log(this.roundsArr);

    })
  }

  getResults() {
    if (this.round == 0 || this.year == 0) {
      alert("select both the values!");
      this.showRows = false;
      return;
    }
    this.showRows = true;
    this.results = [];
    //get the data based on year & round
    this.dataService.getGames().subscribe(temp => {
      temp.forEach(x => {
        if (x.year == this.year && x.round == this.round)
          this.results.push(x)
      })
      console.log(this.results)
    })
  }


}

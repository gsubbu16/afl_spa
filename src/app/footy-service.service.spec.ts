import { TestBed } from '@angular/core/testing';

import { FootyServiceService } from './footy-service.service';

describe('FootyServiceService', () => {
  let service: FootyServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FootyServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

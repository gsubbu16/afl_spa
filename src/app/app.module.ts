import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ViewLeagueTableComponent } from './view-league-table/view-league-table.component';
import { PredictionsComponent } from './predictions/predictions.component';
import { ResultsComponent } from './results/results.component';

import { StatsComponent } from './stats/stats.component';
import { FixturesComponent } from './fixtures/fixtures.component';
import { LadderComponent } from './ladder/ladder.component';
import { StandingsComponent } from './standings/standings.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { RoundResultsComponent } from './round-results/round-results.component';
import { StadiumsComponent } from './stadiums/stadiums.component';


@NgModule({
  declarations: [
    AppComponent,    
    ViewLeagueTableComponent, PredictionsComponent, ResultsComponent, FixturesComponent, StatsComponent, LadderComponent, StandingsComponent, HomeComponent, AboutComponent, RoundResultsComponent, StadiumsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

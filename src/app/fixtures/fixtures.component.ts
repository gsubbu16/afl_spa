import { Component, OnInit } from '@angular/core';
import { FootyServiceService } from '../footy-service.service';

@Component({
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrls: ['./fixtures.component.css']
})
export class FixturesComponent implements OnInit {

  teams = [];
  fixtures = [];

  selectTeam: string = " ";
 // inputDate: Date;
  showRows: boolean = true;

  constructor(private dataService: FootyServiceService) { }

  ngOnInit() {
    this.getTeams();
  }

  getFixtures(): void {

    if (this.selectTeam == " ") {
      alert("Select your team to continue!");
      this.showRows = false;
      return;
    }

    this.showRows = true;
    this.dataService.getGames().subscribe(temp => {

      var next5Matches = [];
      var matches = [];
      var revMatches = [];

      temp.forEach(x => {
        //when team is home team
        if (this.selectTeam == x.hteam && x.round > 19)
          matches.push(x);
        //when team is away team
        if (this.selectTeam == x.ateam && x.round > 19)
          revMatches.push(x);
      });

      let swapteam = " ";

      revMatches.forEach(x => {
        swapteam = x.ateam;
        x.ateam = x.hteam;
        x.hteam = swapteam;
      })

      matches.concat(revMatches);

      //sort by date 
      matches = matches.sort((x, y) => new Date(x.date).getTime() - new Date(y.date).getTime());
      
      //display 5 matches in 2019
      let i = 0;
      matches.forEach(x => {
        if (i < 5 && x.year == 2019) {
          i += 1;
          next5Matches.push(x);
        }
      });
      //push & display
      this.fixtures = next5Matches;
    })
  };

  // compareDate(date1: string, date2: Date): number {
  //   let d1 = new Date(date1);
  //   let d2 = new Date(date2);

  //   if (d1.getTime() === d2.getTime()) return 0;
  //   if (d1 > d2) return 1;
  //   if (d1 < d2) return -1;
  // };

  getTeams() {
    this.dataService.getTeams().subscribe(temp => {
      temp.forEach(x => {
        this.teams.push(x.name);
      })
    })
  }

}

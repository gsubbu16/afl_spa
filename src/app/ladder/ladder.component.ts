import { Component, OnInit } from '@angular/core';
import { FootyServiceService } from '../footy-service.service';

@Component({
  selector: 'app-ladder',
  templateUrl: './ladder.component.html',
  styleUrls: ['./ladder.component.css']
})
export class LadderComponent implements OnInit {

  sourceName:string = " ";
  showRows:boolean = true;

  ladder = [];
  sources = [];

  constructor(private dataService: FootyServiceService ) { }

  ngOnInit(): void {
    this.getSources();
  }

  getLadder(){
    if(this.sourceName == " "){
      alert("Select a source to continue!");
      this.showRows = false;
      return;
    }
    this.showRows = true;
    this.ladder = [];
    this.dataService.getLadder().subscribe(temp=>{
      temp.forEach(x=>{
        if(this.sourceName == x.source)
          this.ladder.push(x);
      })  
      //sort by rank
      this.ladder = this.ladder.sort((x,y)=>x.rank-y.rank);
     
    })
    //console.log(this.sources)
    console.log(this.ladder)
  }

  getSources(){
    this.dataService.getLadder().subscribe(temp=>{
      temp.forEach(x=>{        
          this.sources.push(x.source);
      })     
      //Remove duplicates
      this.sources = this.sources.filter((elem, i, arr) => {
        if (arr.indexOf(elem) === i) {
          return elem
        }
      })
    })
    
  }
  

}

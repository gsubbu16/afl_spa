import { Component, OnInit } from '@angular/core';
import { FootyServiceService } from '../footy-service.service';
import { Tip } from '../tip';

@Component({
  selector: 'app-predictions',
  templateUrl: './predictions.component.html',
  styleUrls: ['./predictions.component.css']
})
export class PredictionsComponent implements OnInit {

  teamName: string = " ";
  showRows: boolean = true;

  tips: Tip[];
  teams = [];

  constructor(private dataService: FootyServiceService) { }

  ngOnInit(): void {
    this.getTeams();
  }

  onChange() {
    this.teamName = this.teamName;
  }

  getTips(): void {

    if (this.teamName == " ") {
      alert("Select a team");
      this.showRows = false;
      return;
    }

    this.showRows = true;
    this.dataService.getTips().subscribe(temp => {

      var filterArray = [];

      temp.forEach(x => {
        if ((x.ateam == this.teamName || x.hteam == this.teamName) && x.round == 20)
          filterArray.push(x);
      })

      this.tips = filterArray;
    });

  }

  getTeams() {
    this.dataService.getTeams().subscribe(temp => {
      temp.forEach(x => {
        this.teams.push(x.name);
      })
    })
  }

}
